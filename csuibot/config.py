from os import environ


APP_ENV = environ.get('APP_ENV', 'development')
DEBUG = environ.get('DEBUG', 'true') == 'true'
TOKEN = '365903690:AAGWEWYWvD5wi8sAQ9xpKa0c2mHRP85PBak'
TELEGRAM_BOT_TOKEN = environ.get('TELEGRAM_BOT_TOKEN', TOKEN)
LOG_LEVEL = environ.get('LOG_LEVEL', 'DEBUG')
WEBHOOK_HOST = environ.get('WEBHOOK_HOST', 'https://katakterbang.herokuapp.com/bot')
WEBHOOK_URL = environ.get('WEBHOOK_URL', 'https://katakterbang.herokuapp.com')
